import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import { Recipe } from "./supporting-files/models";
import { NutrientFact, Product } from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

// Define a type 'CheapestCostItem' representing an item with nutrient facts and its cheapest cost per base unit.
type CheapestCostItem = {
    nutrientFacts: NutrientFact[];
    cheapestCostPerBaseUnit: number;
};
// Takes an array of products and returns the cheapest cost item among them.
/**
 * Takes an array of products and returns the cheapest cost item among them..
 * @param {Product[]} products - List of product
 * @return {CheapestCostItem} 
 * CheapestCost per base unit with nutrientFacts
 */
const getCheapestCostItem = (products: Product[]): CheapestCostItem => {
    // Initialize 'cheapestCostItem' with empty nutrient facts and 0 as the cheapest cost.
    let cheapestCostItem: CheapestCostItem = {
        nutrientFacts: [],
        cheapestCostPerBaseUnit: 0,
    };
    // If the 'products' array is empty or undefined, return the initialized 'cheapestCostItem'.

    if (!products?.length) return cheapestCostItem;
    products.forEach((product) => {
        // Get the nutrient facts for the product in base units and store them in 'nutrientFacts'.
        const nutrientFacts = product.nutrientFacts?.map((fact) => GetNutrientFactInBaseUnits(fact)) || [];
        // Get the cost per base units for the product and store them in 'costPerBaseUnits'.
        const costPerBaseUnits = product.supplierProducts?.map((supplierProduct) => GetCostPerBaseUnit(supplierProduct)) || [];
        // Find the current cheapest cost per base unit among the products.
        const currentCheapestCostPerBaseUnit = Math.min(...costPerBaseUnits);
        // Update 'cheapestCostItem' if the current product has a cheaper cost.
        if (!cheapestCostItem.cheapestCostPerBaseUnit || currentCheapestCostPerBaseUnit < cheapestCostItem.cheapestCostPerBaseUnit) {
            // This step may not be necessary in practice. 
            // I need to add this because the RunTest function is using JSON.stringify to compare two Objects
            // it will return "false" if the two objects are same but the key order is not same.
            const cheapestNutrientFacts = nutrientFacts.sort((a, b) =>
                a.nutrientName.localeCompare(b.nutrientName)
            );
            // Update 'cheapestCostItem' with the current cheapest cost and nutrient facts.
            cheapestCostItem = {
                nutrientFacts: cheapestNutrientFacts,
                cheapestCostPerBaseUnit: currentCheapestCostPerBaseUnit,
            };
        }
    });

    return cheapestCostItem;
};

/**
 * Calculates the summary of a recipe, including nutrients at the cheapest cost and the overall cheapest cost.
 * @param {Recipe} recipe - Recipe to get summary
 * @return {{cheapestCost: number, nutrientsAtCheapestCost: Record<string, NutrientFact}} 
 *         - cheapestCost: cheapest cost that recipe can be made for
 *         - nutrientsAtCheapestCost: summarise its nutritional information
 */
const getRecipeItemSummary = (recipe: Recipe) => {
    // Create an empty object to store nutrient facts at the cheapest cost.
    const nutrientsAtCheapestCost: Record<string, NutrientFact> = {};
    let cheapestCost = 0;

    for (const item of recipe.lineItems) {
        // Get the products for the current ingredient.
        const products = GetProductsForIngredient(item.ingredient);
        // Get the cheapest cost item among the products for the current ingredient.
        const minCostItem = getCheapestCostItem(products);
        for (const nutrientFact of minCostItem.nutrientFacts) {
            // Convert the nutrient fact values to base units.
            const nutrientFactInBaseUnits = GetNutrientFactInBaseUnits(nutrientFact);
            // Check if the nutrient already exists in the 'nutrientsAtCheapestCost' object.
            const existingNutrientFact = nutrientsAtCheapestCost[nutrientFact.nutrientName];

            if (existingNutrientFact) {
                existingNutrientFact.quantityAmount.uomAmount += nutrientFactInBaseUnits.quantityAmount.uomAmount;
            } else {
                nutrientsAtCheapestCost[nutrientFact.nutrientName] = nutrientFactInBaseUnits;
            }
        }
        // Calculate the cost for the current ingredient and add it to the overall cheapest cost.
        cheapestCost += item.unitOfMeasure.uomAmount * minCostItem.cheapestCostPerBaseUnit;
    }

    return {
        cheapestCost,
        nutrientsAtCheapestCost,
    };
};



// Loop through each recipe in the 'recipeData' array and calculate its summary using 'getRecipeItemSummary'.
// Store the result in the 'recipeSummary' object with the recipe name as the key.
recipeData.forEach((recipe) => {
    recipeSummary[recipe.recipeName] = getRecipeItemSummary(recipe);
});



/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
